var check_interval = setInterval(check_server, 2000);
var options = {};
var server_status_yubo = 0;
var server_status_gingo = 0;
var server_status_atys = 0;
var server_down_timer_yubo = 0;
var server_down_timer_gingo = 0;
var server_down_timer_atys = 0;

function check_server() {
	//here load the Server we want check
	load('yubo');
	load('gingo');
	load('atys');
}

function load(server) {
	$.ajax({
		url: "https://app.ryzom.com/app_server_status/data/" + server + "_status.json?randval="+Math.random(),
		dataType: "json",
		success: function (result) {
			obj = result;
			new_content = "";

			if (server == "yubo") {
				server_status = server_status_yubo;
				server_down_timer = server_down_timer_yubo;

			} else if (server == "gingo") {
				server_status = server_status_gingo;
				server_down_timer = server_down_timer_gingo;
			}else{
				server_status = server_status_atys;
				server_down_timer = server_down_timer_atys;
			}

			//console.log(obj.state +" "+ server+" "+ server_status);
			//console.log("Server ON" + server);
				var state = obj.state
				var ais_ark = obj.ais_ark
				var ais_fyros = obj.ais_fyros
				var ais_matis = obj.ais_matis
				var ais_newbieland = obj.ais_newbieland
				var ais_roots = obj.ais_roots
				var ais_tryker = obj.ais_tryker
				var ais_zorai = obj.ais_zorai
				var egs = obj.egs
				var ios = obj.ios

				var services_crash = 0;
				var services_crash_name = "";

				if (ais_ark === "online") {
					new_content = new_content + '<tr><td><img src="https://app.ryzom.com/data/icons/16/bullet_green.png"> <font size="4"><strong>AIS Ark</strong></font></td></tr>'
				} else {
					if(state != "up" && server_down_timer <= 15){
						new_content = new_content + '<tr><td><img src="https://app.ryzom.com/data/icons/16/bullet_green.png"> <font size="4"><strong>AIS Ark</strong></font></td></tr>'
					}else{
						new_content = new_content + '<tr><td><img src="https://app.ryzom.com/data/icons/16/bullet_red.png"> <font size="4"><strong>AIS Ark (' + ais_ark + ')</strong></font></td></tr>'
						services_crash_name = services_crash_name + "Ark"
					}

					services_crash++
				}

				if (ais_fyros === "online") {
					new_content = new_content + '<tr><td><img src="https://app.ryzom.com/data/icons/16/bullet_green.png"> <font size="4"><strong>AIS Fyros</strong></font></td></tr>'
				} else {
					if(state != "up" && server_down_timer <= 15){
						new_content = new_content + '<tr><td><img src="https://app.ryzom.com/data/icons/16/bullet_green.png"> <font size="4"><strong>AIS Fyros</strong></font></td></tr>'
					}else{
						new_content = new_content + '<tr><td><img src="https://app.ryzom.com/data/icons/16/bullet_red.png"> <font size="4"><strong>AIS Fyros (' + ais_fyros + ')</strong></font></td></tr>'
						services_crash_name = services_crash_name + " Fyros"
					}

					services_crash++
				}

				if (ais_matis === "online") {
					new_content = new_content + '<tr><td><img src="https://app.ryzom.com/data/icons/16/bullet_green.png"> <font size="4"><strong>AIS Matis</strong></font></td></tr>'
				} else {
					if(state != "up" && server_down_timer <= 15){
						new_content = new_content + '<tr><td><img src="https://app.ryzom.com/data/icons/16/bullet_green.png"> <font size="4"><strong>AIS Matis</strong></font></td></tr>'
					}else{
						new_content = new_content + '<tr><td><img src="https://app.ryzom.com/data/icons/16/bullet_red.png"> <font size="4"><strong>AIS Matis (' + ais_matis + ')</strong></font></td></tr>'
						services_crash_name = services_crash_name + " Matis"
					}

					services_crash++
				}

				if (ais_newbieland === "online") {
					new_content = new_content + '<tr><td><img src="https://app.ryzom.com/data/icons/16/bullet_green.png"> <font size="4"><strong>AIS Newbieland</strong></font></td></tr>'
				} else {
					if(state != "up" && server_down_timer <= 15){
						new_content = new_content + '<tr><td><img src="https://app.ryzom.com/data/icons/16/bullet_green.png"> <font size="4"><strong>AIS Newbieland</strong></font></td></tr>'
					}else{
						new_content = new_content + '<tr><td><img src="https://app.ryzom.com/data/icons/16/bullet_red.png"> <font size="4"><strong>AIS Newbieland (' + ais_newbieland + ')</strong></font></td></tr>'
						services_crash_name = services_crash_name + " Newbieland"
					}

					services_crash++
				}

				if (ais_roots === "online") {
					new_content = new_content + '<tr><td><img src="https://app.ryzom.com/data/icons/16/bullet_green.png"> <font size="4"><strong>AIS Roots</strong></font></td></tr>'
				} else {
					if(state != "up" && server_down_timer <= 15){
						new_content = new_content + '<tr><td><img src="https://app.ryzom.com/data/icons/16/bullet_green.png"> <font size="4"><strong>AIS Roots</strong></font></td></tr>'
					}else{
						new_content = new_content + '<tr><td><img src="https://app.ryzom.com/data/icons/16/bullet_red.png"> <font size="4"><strong>AIS Roots (' + ais_roots + ')</strong></font></td></tr>'
						services_crash_name = services_crash_name + " Roots"
					}

					services_crash++
				}

				if (ais_tryker === "online") {
					new_content = new_content + '<tr><td><img src="https://app.ryzom.com/data/icons/16/bullet_green.png"> <font size="4"><strong>AIS Tryker</strong></font></td></tr>'
				} else {
					if(state != "up" && server_down_timer <= 15){
						new_content = new_content + '<tr><td><img src="https://app.ryzom.com/data/icons/16/bullet_green.png"> <font size="4"><strong>AIS Tryker</strong></font></td></tr>'
					}else{
						new_content = new_content + '<tr><td><img src="https://app.ryzom.com/data/icons/16/bullet_red.png"> <font size="4"><strong>AIS Tryker (' + ais_tryker + ')</strong></font></td></tr>'
						services_crash_name = services_crash_name + " Tryker"
					}

					services_crash++
				}

				if (ais_zorai === "online") {
					new_content = new_content + '<tr><td><img src="https://app.ryzom.com/data/icons/16/bullet_green.png"> <font size="4"><strong>AIS Zorai</strong></font></td></tr>'
				} else {
					if(state != "up" && server_down_timer <= 15){
						new_content = new_content + '<tr><td><img src="https://app.ryzom.com/data/icons/16/bullet_green.png"> <font size="4"><strong>AIS Zorai</strong></font></td></tr>'
					}else{
						new_content = new_content + '<tr><td><img src="https://app.ryzom.com/data/icons/16/bullet_red.png"> <font size="4"><strong>AIS Zorai (' + ais_zorai + ')</strong></font></td></tr>'
						services_crash_name = services_crash_name + " Zorai"
					}

					services_crash++
				}

				if (egs === "online") {
					new_content = new_content + '<tr><td><img src="https://app.ryzom.com/data/icons/16/bullet_green.png"> <font size="4"><strong>EGS</strong></font></td></tr>'
				} else {
					if(state != "up" && server_down_timer <= 15){
						new_content = new_content + '<tr><td><img src="https://app.ryzom.com/data/icons/16/bullet_green.png"> <font size="4"><strong>EGS</strong></font></td></tr>'
					}else{
						new_content = new_content + '<tr><td><img src="https://app.ryzom.com/data/icons/16/bullet_red.png"> <font size="4"><strong>EGS (' + egs + ')</strong></font></td></tr>'
						services_crash_name = services_crash_name + " Egs"
					}

					services_crash++
				}

				if (ios === "online") {
					new_content = new_content + '<tr><td><img src="https://app.ryzom.com/data/icons/16/bullet_green.png"> <font size="4"><strong>IOS</strong></font></td></tr>'
				} else {
					if(state != "up" && server_down_timer <= 15){
						 new_content = new_content + '<tr><td><img src="https://app.ryzom.com/data/icons/16/bullet_green.png"> <font size="4"><strong>IOS</strong></font></td></tr>'
					}else{
						new_content = new_content + '<tr><td><img src="https://app.ryzom.com/data/icons/16/bullet_red.png"> <font size="4"><strong>IOS (' + ios + ')</strong></font></td></tr>'
						services_crash_name = services_crash_name + " IOS"
					}

					services_crash++
				}


				if (state == "ras") {
					$('#' + server + '_state').html('<img src="https://app.ryzom.com/data/icons/32/bullet_black.png">');
					new_content = '<tr><td><font size="4" color="red">No information from RAS. Maybe the Shard are down?</font></td></tr>';
				} else if (state == "down") {
					$('#' + server + '_state').html('<img src="https://app.ryzom.com/data/icons/32/bullet_black.png">');
					new_content = '<tr><td><font size="4" color="red">Server is down</font></td></tr>';
				} else
				 {

					if (services_crash != 0 && state == "up") {

						if (server_status == 0) {
							config_notify(server, 'https://app.ryzom.com/data/icons/32/exclamation.png', 'Service ' + services_crash_name + ' crash');

							if (server == "yubo") {
								server_status_yubo = 1;
							} else if(server == "gingo") {
								server_status_gingo = 1;
							}else{
								server_status_atys = 1;
							}
						}

						$('#' + server + '_state').html('<img src="https://app.ryzom.com/data/icons/32/bullet_blue.png">');
					}

					 if (services_crash == 0 && state == "up") {
						if (server_status == 1) {
							config_notify(server, 'https://app.ryzom.com/data/icons/32/connect.png', 'Server back Online!');

							if (server == "yubo") {
								server_status_yubo = 0;
							} else if(server == "gingo") {
								server_status_gingo = 0;
							} else {
								server_status_atys = 0;
							}
						}

						if (server == "yubo") {
							server_down_timer_yubo = 0;
						} else if(server == "gingo") {
							server_down_timer_gingo = 0;
						} else {
							server_down_timer_atys = 0;
						}

						$('#' + server + '_state').html('<img src="https://app.ryzom.com/data/icons/32/bullet_green.png">');
					}

					if (services_crash == 9) {
						//console.log("Server OFF" + server);
						if(server_down_timer > 30) {

							$('#' + server + '_state').html('<img src="https://app.ryzom.com/data/icons/32/bullet_red.png">');
							new_content = '<tr><td><font size="4" color="red">Wait for ' +server+ ' UP</font></td></tr>';

							if (server_status == 0) {
								config_notify(server, 'https://app.ryzom.com/data/icons/32/end.png', 'Server Offline');

								if (server == "yubo") {
									server_status_yubo = 1;
								} else if(server == "gingo") {
									server_status_gingo = 1;
								} else {
									server_status_atys = 1;
								}
							}
						} else {
							if (server == "yubo") {
								server_down_timer_yubo++;
							} else if(server == "gingo") {
								server_down_timer_gingo++;
							} else {
								server_down_timer_atys++;
							}
						}
					}
				}

				$('#' + server + '_result').html(new_content);
		}
	});
}

//clearInterval(check_interval);

function config_notify(server, icon, message) {
	options = {
		body: message,
		icon: icon
	}
	notifyMe(server);
}

function notifyMe(server) {
	if (!("Notification" in window)) {
		alert("This browser does not support desktop notification");
	} else if (Notification.permission === "granted") {
		var notification = new Notification("Server status " + server, options);
	} else if (Notification.permission !== 'denied') {
		Notification.requestPermission(function (permission) {
			var notification = new Notification("Server status " + server, options);
		}
		)
	}
}
