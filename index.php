<?php

/* Copyright (C) 2012 Winch Gate Property Limited
 * 
 * This file is part of ryzom_app.
 * ryzom_app is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ryzom_app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ryzom_app.  If not, see <http://www.gnu.org/licenses/>.
 */

define('APP_NAME', 'app_server_status');

include_once('../config.php');
include_once(RYAPP_PATH . '/lang.php');

define('APP_PATH', RYAPP_PATH . APP_NAME);
define('APP_URL', RYAPP_URL . APP_NAME . '/');

ryzom_auth_user(false);

$c = "";

if ($ig) {
    $c .= "<h2><font color=red>" . _t('not_acces_ig') . "</font></h2>";
} else {
	
    $c .= '<table width="100%">';
    $c .= '<tr><td style=“white-space:nowrap;“>';


//Yubo DEV Server

    $c .= '<table>';
    $c .= '<tr><td valign="top" width="24"><div id="yubo_state"><img src="https://app.ryzom.com/data/icons/32/bullet_red.png"></div></td><td><h1>Yubo DEV:</h1></td></tr>';
    $c .= '<tr><td colspan="2"><div id="yubo_result"></div></td></tr>';
    $c .= '</table>';


    $c .= '</td><td style=“white-space:nowrap;“>';
	
	//Gingo Test Server

    $c .= '<table>';
    $c .= '<tr><td valign="top" width="24"><div id="gingo_state"><img src="https://app.ryzom.com/data/icons/32/bullet_red.png"></div></td><td><h1>Gingo Test:</h1></td></tr>';
    $c .= '<tr><td colspan="2"><div id="gingo_result"></div></td></tr>';
    $c .= '</table>';


    $c .= '</td><td style=“white-space:nowrap;“>';


//Atys Live Server
    $c .= '<table>';
    $c .= '<tr><td valign="top" width="24"><div id="atys_state"><img src="https://app.ryzom.com/data/icons/32/bullet_red.png"></div></td><td><h1>Atys Live:</h1></td></tr>';
    $c .= '<tr><td colspan="2"><div id="atys_result"></div></td></tr>';
    $c .= '</table>';


    $c .= '</td></tr>';
    $c .= '</table>';
	
}

echo ryzom_app_render(APP_NAME, $c, '', array(
    APP_URL . 'data/jquery-3.2.1.min.js',
    APP_URL . 'data/send_notify.js',
    APP_URL . 'node_modules/crypto-js/crypto-js.js',
));
